# CI/CD @ Azure workshop

### docs
Documentation (NL + EN) formatted in markdown.

### scripts
Various shell scripts to speed up the work.
You can run them from an Azure bash cloudshell.
See documentation for instructions.
