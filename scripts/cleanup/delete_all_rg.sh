for subscription in `cat subs.txt`; do 
   echo "setting subscription to: ${subscription}"
   az account set --subscription "${subscription}"
   for rg in `az group list --query "[].{name:name}" --output tsv`; do 
      echo "Deleting resource group: ${rg}"
      az group delete --name ${rg} --yes --no-wait
   done
   for id in `az ad app list --all |  jq '.[].appId' | tr -d '"'`; do 
      echo "Deleting App registrations: ${id}"
      az ad app delete --id ${id}
   done
done
