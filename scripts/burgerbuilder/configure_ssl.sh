#!/bin/bash
#
# This script will
# - Ask for servicename, portnumber and namespace of the running container
# - Configure an SSL-certificate (let's encrypt)
# - Create an ingress controller for the service at https://frontend.<EXTERNALIP>.nip.io

DONEDIR="$(pwd)/.done"
mkdir -p "$DONEDIR"
LOG="$(pwd)/configure_ssl.log"
export PR=$(cat $DONEDIR/prefix 2>/dev/null)

if [ ! -e $DONEDIR/${PR}_create_aks.done ]
then
  echo "Kubernetes cluster not available"
  exit 99
fi

export SVCNAME=$(cat $DONEDIR/svcname 2>/dev/null)
export SVCPORT=$(cat $DONEDIR/svcport 2>/dev/null)
export SVCNAMESPACE=$(cat $DONEDIR/svcnamespace 2>/dev/null)

if [ -n "$SVCNAME" -a -n "$SVCPORT" ]
then
  echo "Existing service name found: $SVCNAME"
  echo "Existing service port number found: $SVCPORT"
  echo "Existing service namespace found: $SVCNAMESPACE"
  echo -en "\nUse these settings? (Y/N) "
  read REUSE
fi

if  ! echo "$REUSE" | grep -Eq "J|j|Y|y"
then

  echo -en "Enter service name in aks-cluster: "
  read SVCNAME
  echo -en "Enter port number for that service: "
  read SVCPORT
  echo -en "Enter kubernetes namespace for that service: "
  read SVCNAMESPACE

  echo -en "\nContinue? (Y/N) "
  read ANSWER

  if ! echo "$ANSWER" | grep -Eq "Y|y|J|j"
  then
    echo -e "\nThat was close :-)"
    exit 98
  fi

  echo "$SVCNAME" >$DONEDIR/svcname
  echo "$SVCPORT" >$DONEDIR/svcport
  echo "$SVCNAMESPACE" >$DONEDIR/svcnamespace

fi


# Deploy frontend service
echo -en "Deploy frontend service ... "
if [ -e "${DONEDIR}/${PR}_ssl_frontend.done" ]
then
  echo "SKIP"
else
  cat templates/frontend-service.yml_template | sed "s/_SVCPORT_/${SVCPORT}/g" >templates/frontend-service.yml
  echo "* executing: kubectl apply -f templates/frontend-service.yml -n \"$SVCNAMESPACE\"" >>$LOG
  if kubectl apply -f templates/frontend-service.yml -n "$SVCNAMESPACE" >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_ssl_frontend.done
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi


# Configure helmchart repositories
echo -en "Configure helm-chart repositories ... "
if [ -e "${DONEDIR}/${PR}_ssl_repo.done" ]
then
  echo "SKIP"
else
  echo "* executing: helm repo add stable https://kubernetes-charts.storage.googleapis.com && helm repo add jetstack https://charts.jetstack.io && helm repo update" >>$LOG
  if helm repo add stable https://kubernetes-charts.storage.googleapis.com >>$LOG 2>&1 && \
     helm repo add jetstack https://charts.jetstack.io >>$LOG 2>&1 && \
     helm repo update  >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_ssl_repo.done
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi


# Deploy nginx ingress controller
echo -en "Deploy nginx ingress controller ... "
if [ -e "${DONEDIR}/${PR}_ssl_nginx.done" ]
then
  echo "SKIP"
else
  echo "* executing: kubectl create namespace ingress && helm upgrade --install ingress stable/nginx-ingress --namespace ingress" >>$LOG
  if kubectl create namespace ingress >>$LOG 2>&1 && \
     helm upgrade --install ingress stable/nginx-ingress --namespace ingress >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_ssl_nginx.done
    echo "OK"
    echo -en "Wait for 30 seconds, to let it settle ... "
    sleep 30 # It takes a few seconds ...
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi


echo -en "Retrieve external IP-address ... "
EXTERNALIP=$(kubectl get svc -n ingress ingress-nginx-ingress-controller -o jsonpath="{.status.loadBalancer.ingress[*].ip}" 2>>$LOG)
if [ -n "$EXTERNALIP" ]
then
  echo "OK ($EXTERNALIP)"
else
  echo "FAIL: could not retrieve external IP, check log $LOG"
  exit 1
fi


# Create ingress resource
echo -en "Create ingress resource ... "
if [ -e "${DONEDIR}/${PR}_ssl_ingress.done" ]
then
  echo "SKIP"
else
  cat templates/frontend-ingress.yml_template | sed "s/_EXTERNALIP_/${EXTERNALIP}/g" | \
    sed "s/_SERVICENAME_/${SVCNAME}/g" | sed "s/_SERVICEPORT_/${SVCPORT}/g" >templates/frontend-ingress.yml
  echo "* executing: kubectl apply -f templates/frontend-ingress.yml -n \"$SVCNAMESPACE\"" >>$LOG
  if kubectl apply -f templates/frontend-ingress.yml -n "$SVCNAMESPACE" >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_ssl_ingress.done
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi


# Install cert-manager prerequisites
echo -en "Install certificate manager prerequisites... "
if [ -e "${DONEDIR}/${PR}_ssl_certman_prereqs.done" ]
then
  echo "SKIP"
else
  echo "* executing: kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.12/deploy/manifests/00-crds.yaml -n \"$SVCNAMESPACE\"" >>$LOG
  if kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.12/deploy/manifests/00-crds.yaml -n "$SVCNAMESPACE" >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_ssl_certman_prereqs.done
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi


# Install certificate manager
echo -en "Install certificate manager ... "
if [ -e "${DONEDIR}/${PR}_ssl_certman.done" ]
then
  echo "SKIP"
else
  echo "* executing:  helm install cert-manager jetstack/cert-manager --set ingressShim.defaultIssuerName=letsencrypt --set ingressShim.defaultIssuerKind=ClusterIssuer --version v0.12.0" >>$LOG
  if helm install cert-manager jetstack/cert-manager --set ingressShim.defaultIssuerName=letsencrypt --set ingressShim.defaultIssuerKind=ClusterIssuer --version v0.12.0 >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_ssl_certman.done
    echo "OK"
    echo -en "Wait for a minute to let it settle ... "
    sleep 60 # It takes a few seconds ...
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi


# Issue certificate
echo -en "Issue certificate ... "
if [ -e "${DONEDIR}/${PR}_ssl_issuecert.done" ]
then
  echo "SKIP"
else
  echo "* executing: kubectl apply -f templates/clusterissuer.yml -n \"$SVCNAMESPACE\"" >>$LOG
  if kubectl apply -f templates/clusterissuer.yml -n "$SVCNAMESPACE" >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_ssl_issuecert.done
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi


# Create ingress resource
echo -en "Create ingress TLS resource ... "
if [ -e "${DONEDIR}/${PR}_ssl_ingresstls.done" ]
then
  echo "SKIP"
else
  cat templates/frontend-ingress-tls.yml_template | sed "s/_EXTERNALIP_/${EXTERNALIP}/g" | \
    sed "s/_SERVICENAME_/${SVCNAME}/g" | sed "s/_SERVICEPORT_/${SVCPORT}/g" >templates/frontend-ingress-tls.yml
  echo "* executing: kubectl apply -f templates/frontend-ingress-tls.yml -n \"$SVCNAMESPACE\"" >>$LOG
  if kubectl apply -f templates/frontend-ingress-tls.yml -n "$SVCNAMESPACE" >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_ssl_ingresstls.done
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi


echo -e "\nBrowse to https://frontend._EXTERNALIP_.nip.io" | sed "s/_EXTERNALIP_/${EXTERNALIP}/g"


exit 0
