#!/bin/bash
#
# This script will remove the following resources, which are
# create by the create_aks.sh script:
# - kubernetes credentials
# - kubernetes cluster
# - acr (container registry)
# - resource group

DONEDIR="$(pwd)/.done"
LOG="$(pwd)/remove_aks.log"
echo -e "===== $(date) =====\n" >>$LOG

if [ ! -e $DONEDIR/prefix -o ! -e $DONEDIR/location ]
then
  echo "$DONEDIR/prefix and/or $DONEDIR/location not present"
  exit 99
fi

PR=$(cat $DONEDIR/prefix 2>/dev/null)
LOC=$(cat $DONEDIR/location 2>/dev/null)

if [ -z "$PR" -o -z "$LOC" ]
then
  echo "Location and/or prefix have no content"
  exit 99
fi

echo "About to remove:"
echo "- Kubernetes cluster ${PR}aks"
echo "- Container registory ${PR}acr"
echo "- Resource group ${PR}group"
echo -en "\nAre you sure? (Y/N) "
read ANSWER

if ! echo "$ANSWER" | grep -Eq "Y|y|J|j"
then
  echo -e "\nThat was close :-)"
  exit 98
fi

echo -en "\nRemoving kubectl configuration for cluster ${PR}aks ... "
if [ -e $DONEDIR/${PR}_remove_kubeconfig.done ]
then
  echo "SKIP"
else
  if [ -e $DONEDIR/${PR}_create_kubeconfig.done ]
  then
    if kubectl config delete-cluster "${PR}aks" >>$LOG 2>&1 && \
       kubectl config delete-context "${PR}aks" >>$LOG 2>&1 && \
       kubectl config unset "users.clusterUser_${PR}group_${PR}aks" >>$LOG 2>&1
    then
      touch $DONEDIR/${PR}_remove_kubeconfig.done
      rm -f $DONEDIR/${PR}_create_kubeconfig.done
      echo "OK"
    else
      echo "FAIL, check log $LOG"
      exit 1
    fi
  else
    echo "SKIP"
  fi
fi

echo -en "Removing kubernetes cluster ${PR}aks ... "
if [ -e $DONEDIR/${PR}_remove_aks.done ]
then
  echo "SKIP"
else
  if [ -e $DONEDIR/${PR}_create_aks.done ]
  then
    if az aks delete --yes --resource-group "${PR}group" --name "${PR}aks" >>$LOG 2>&1
    then
      touch $DONEDIR/${PR}_remove_aks.done
      rm -f $DONEDIR/${PR}_create_aks.done ${DONEDIR}/${PR}_create_aksdashboard.done
      echo "OK"
    else
      echo "FAIL, check log $LOG"
      exit 1
    fi
  else
    echo "SKIP"
  fi
fi

echo -en "Removing container registry ${PR}acr ... "
if [ -e $DONEDIR/${PR}_remove_acr.done ]
then
  echo "SKIP"
else
  if [ -e $DONEDIR/${PR}_create_acr.done ]
  then
    if az acr delete --name "${PR}acr" --resource-group "${PR}group" >>$LOG 2>&1
    then
      touch $DONEDIR/${PR}_remove_acr.done
      rm -f $DONEDIR/${PR}_create_acr.done
      echo "OK"
    else
      echo "FAIL, check log $LOG"
      exit 1
    fi
  else
    echo "SKIP"
  fi
fi

echo -en "Removing resource group ${PR}group ... "
if [ -e $DONEDIR/${PR}_remove_resourcegroup.done ]
then
  echo "SKIP"
else
  if [ -e $DONEDIR/${PR}_create_resourcegroup.done ]
  then
    if az group delete --yes --name "${PR}group" >>$LOG 2>&1
    then
      touch $DONEDIR/${PR}_remove_resourcegroup.done
      rm -f $DONEDIR/${PR}_create_resourcegroup.done
      echo "OK"
    else
      echo "FAIL, check log $LOG"
      exit 1
    fi
  else
    echo "SKIP"
  fi
fi

exit 0
