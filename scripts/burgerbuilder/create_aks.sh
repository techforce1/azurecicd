#!/bin/bash
#
# This script will
# - ask for location and prefix, and check if the location is fit for the job
# - create a resource group
# - create a container registry
# - create a kubernetes cluster
# - retrieve kubectl credentials for the kubernetes cluster

DONEDIR="$(pwd)/.done"
mkdir -p "$DONEDIR"
LOG="$(pwd)/create_aks.log"
VMSIZE="Standard_DS2_v2"

PR=$(cat $DONEDIR/prefix 2>/dev/null)
LOC=$(cat $DONEDIR/location 2>/dev/null)

if [ -n "$PR" -a -n "$LOC" ]
then
  echo "Existing location found: $LOC"
  echo "Existing prefix found: $PR"
  echo -en "\nUse these settings? (Y/N) "
  read REUSE
fi

if  ! echo "$REUSE" | grep -Eq "J|j|Y|y"
then

  echo -en "\nEnter location (enter to list locations): "
  read LOC
  if [ -z "$LOC" ]
  then
    echo
    # Show only locations supporting the right VM type
    for LOC in $(az account list-locations | jq '.[] | .name' | tr -d '"')
    do
      az vm list-sizes --location "$LOC" --output table 2>/dev/null | grep -q " ${VMSIZE} " && echo $LOC
    done
    exit 99
  fi

  if ! az account list-locations | jq '.[] | .name' | tr -d '"' | grep -qx "$LOC"
  then
    echo -e "\n$LOC is an invalid location."
    echo -e "Valid locations are:\n"
    az account list-locations --output table
    exit 99
  fi

  echo -en "\nEnter prefix for resources to be created: "
  read PR

fi

if [ -z "$PR" ] || echo "$PR" | grep -q ' ' 
then
  # Prefix should not be empty and should not contain spaces
  echo "Invalid prefix: $PR"
  exit 99
fi

if ! az vm list-sizes --location "$LOC" --output table 2>/dev/null | grep -q " ${VMSIZE} "
then
  echo "Unfortunately the VM type $VMSIZE is not available in $LOC, please try another location."
  exit 98
fi

echo "$PR" >${DONEDIR}/prefix
echo "$LOC" >${DONEDIR}/location

# Create resource-group
# Are resourcegroup name must be globally unique in your account
echo -en "\nCreating resource group ${PR}group in location ${LOC} ... "
if [ -e "${DONEDIR}/${PR}_create_resourcegroup.done" ]
then
  echo "SKIP"
else
  echo "* executing: az group create --name \"${PR}group\" --location \"${LOC}\"" >>$LOG
  if az group create --name "${PR}group" --location "${LOC}" >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_create_resourcegroup.done
    rm -f $DONEDIR/${PR}_remove_resourcegroup.done
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi

# Create container registry
echo -n "Creating Azure Container Registry ${PR}acr ... "
if [ -e "${DONEDIR}/${PR}_create_acr.done" ]
then
  echo "SKIP"
else
  echo "* executing: az acr create --resource-group \"${PR}group\" --name \"${PR}acr\" --sku Basic" >>$LOG
  if az acr create --resource-group "${PR}group" --name "${PR}acr" --sku Basic >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_create_acr.done
    rm -f $DONEDIR/${PR}_remove_acr.done
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi

# Get most recent Kubernetes version, based on $RELEASE, for that location
RELEASE="1.15"
VERSION=$(az aks get-versions -l ${LOC} --query 'orchestrators[].orchestratorVersion' -o tsv | sort | grep "${RELEASE}\." | tail -n 1)

# Create Kubernetes cluster (5 - 10 minutes)
echo -n "Creating Kubernetes cluster ${PR}aks (this may take a while) ... "
if [ -e "${DONEDIR}/${PR}_create_aks.done" ]
then
  echo "SKIP"
else
  echo "* executing: az aks create --resource-group \"${PR}group\" --kubernetes-version \"$VERSION\" --name \"${PR}aks\" --node-count 2 --generate-ssh-keys --attach-acr \"${PR}acr\"" >>$LOG
  if az aks create --resource-group "${PR}group" --kubernetes-version "$VERSION" --name "${PR}aks" --node-count 2 --generate-ssh-keys --attach-acr "${PR}acr" >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_create_aks.done
    rm -f $DONEDIR/${PR}_remove_aks.done
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi

# Get Kubernetes credentials for use with kubectl
echo -n "Retrieving Kubernetes credentials ... "
if [ -e "${DONEDIR}/${PR}_create_kubeconfig.done" ]
then
  echo "SKIP"
else
  echo "* executing: az aks get-credentials --resource-group \"${PR}group\" --name \"${PR}aks\"" >>$LOG
  if az aks get-credentials --resource-group "${PR}group" --name "${PR}aks" >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_create_kubeconfig.done
    rm -f $DONEDIR/${PR}_remove_kubeconfig.done
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi

# Configure access to Kubernetes dashboard
echo -n "Configuring access to Kubernetes dashboard ... "
if [ -e "${DONEDIR}/${PR}_create_aksdashboard.done" ]
then
  echo "SKIP"
else
  echo "* executing: kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard" >>$LOG
  if kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard >>$LOG 2>&1
  then
    touch ${DONEDIR}/${PR}_create_aksdashboard.done
    rm -f $DONEDIR/${PR}_remove_aksdashboard.done
    echo "OK"
  else
    echo "FAIL, check log $LOG"
    exit 1
  fi
fi

echo -e "\nExecute the following command to get access to the Kubernetes dashboard:"
echo "az aks browse --name \"${PR}aks\" --resource-group \"${PR}group\""

exit 0
