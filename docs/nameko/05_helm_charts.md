## Opdracht 4: deploy Helm charts via Azure DevOps pipeline

Na succesvol installeren van de dependencies in de vorige stap gaan we nu de gebouwde applicatie componenten deployen middels de Helm charts in het repository.

- Voeg een Helm task toe aan de pipeline en deploy het Products component:
    - Selecteer de juiste Azure Subscription
    - Selecteer de juiste resource group
    - Selecteer de juiste cluster
    - Vul de rest v/d argumenten in, paar kleine hints/tips:
        - Voor de Helm charts zelf *niet* de letterlijke chart maar de *basedir* van de chart kiezen
        - De volledige stap is een upgrade actie die overeen moet komen met het volgende CLI commando:  
        `cd k8s; helm upgrade gateway charts/gateway --install --namespace=examples --set image.tag=latest`

- Herhaal dezelfde stappen voor de *orders* en *products* componenten.



