## Opdracht 1: Nameko base pipeline

In deze eerste opdracht gaan we de bestaande build bekijken en een eerste conversie naar een Azure DevOps pipeline maken. 

- Login op Azure DevOps met de verstrekte credentials https://dev.azure.com
- Begin met het importeren van het https://gitlab.com/techforce1/nameko-azuredevops.git repository naar een repo in Azure DevOps:
     - In het keuzemenu aan de linkerkant "Repos"
     - Dropdown box bovenaan de pagina aan het eind v/d breadcrumbs aanklikken en "Import Repository" kiezen
     - Vul type Git en de URL hierboven in en wacht tot de import klaar is.
  
- Vervolgens gaan we een eerste basic pipeline bouwen en koppelen aan het repository. De bestaande build wordt aangedreven met een Makefile, open deze om de verschillende stappen en afhankelijkheden van de build te zien. Verder zitten er gelaagde Docker image builds in het repo, waarbij bepaalde baseimages als basis voor de rest gebruikt worden, dus kijk ook de verschillende Docker files door om de structuur in images te zien. De README.md geeft een goede high-level overview van de verschillende componenten en hoe ze onderling communiceren. 
We gaan in de opdrachten voor dit project uit van de "Classic"  pipeline in Azure DevOps, ook de YAML variant is mogelijk (qua opties gelijk) maar de beschrijving gaat uit van de Classic Editor 

- Opdracht 1: bouw een Azure DevOps pipeline die de eerste dependencie bouwt en naar ACR pushed

     - Ga naar het Pipelines menu en kies de optie "New pipeline"
     - Kies de Classic Editor variant, selecteer Azure Repos Git als source en vul de naam in van het eerder geimporteerde repo (nameko-azdevops), branche master
     - Kies de Docker Container template als startpunt voor de pipeline, na het selecteren creert deze template twee pipeline stappen:
     - Build an image: bouwt Docker image volgens specs
     - Push an image: pushed een of meerdere gebouwde images naar beschikbare ACR

Lees het Makefile en de verschillende Dockerfiles en bepaal de image dependencies voor de eerste base image build step, nameko-example-base. Selecteer beschikbare Azure Container Registry (zoals gebruikt in de het Burger Builder project), Azure Subscription (idem) en vul de build-arguments aan op basis van het bestaande Makefile.

Doe hetzelfde voor de tweede base image, nameko-example-builder.

Aan het eind van deze opdracht heb je een pipeline gemaakt met 4 onderdelen:
- Bouw base image
- Push base image
- Bouw builder image
- Push builder image







   

