## Nameko 
**Python based microservices**

Gebaseerd op een voorbeeld project van het Nameko microservices framework gaan we nu een iets geavanceerder project opzetten en deployen op de al gedefinieerde AKS cluster.

### Doel

- Opzetten van een pipeline voor een project met meerdere microservices en componenten
- Het converteren van de bestaande build stappen naar Azure DevOps build stappen
- De componenten inclusief hun dependencies deployen naar AKS 
- De hele keten testen via REST calls 

Benodigde links:
- [Techforce1 Nameko repo](https://gitlab.com/techforce1/nameko-azuredevops.git)
- [Azure DevOps portal](https://dev.azure.com)
- [Azure portal](https://portal.azure.com/#home)
- [Azure cloudshell info](https://gitlab.com/techforce1/azurecicd/blob/master/docs/cloudshell.md)







