# Opdracht 5: test de volledige applicatie REST endpoints via HTTP

All components should now be running and communicating on your AKS cluster (verify using kubectl as needed)

Follow up with:
- enable http routing on your AKS cluster (let op: dit duurt even!) by running Azure CLI commands:
- az aks create --resource-group [resourcegroup] --name [aksname] --enable-addons http_application_routing
- note the output
- enable HTTP routing in your Azure Portal through resources -> AKS Clusters

Test the back-end: 


Create a product:
- $ curl -XPOST -d '{"id": "the_odyssey", "title": "The Odyssey", "passenger_capacity": 101, "maximum_speed": 5, "in_stock": 10}' 'http://[[CLUSTER INGRESS IP]/products'

Request a product::
$ curl 'http://[[CLUSTERIP]]/products/the_odyssey'

response should be similar to:

{
  "id": "the_odyssey",
  "title": "The Odyssey",
  "passenger_capacity": 101,
  "maximum_speed": 5,
  "in_stock": 10
}

- create an order:
  
$ curl -XPOST -d '{"order_details": [{"product_id": "the_odyssey", "price": "100000.99", "quantity": 1}]}' 'http://[[CLSUTERIP]]/orders'

- request an order:

$ curl 'http://[[CLUSTERIP]]/orders/1'

response should be similar to:

{
  "id": 1,
  "order_details": [
    {
      "id": 1,
      "quantity": 1,
      "product_id": "the_odyssey",
      "image": "http://www.example.com/airship/images/the_odyssey.jpg",
      "price": "100000.99",
      "product": {
        "maximum_speed": 5,
        "id": "the_odyssey",
        "title": "The Odyssey",
        "passenger_capacity": 101,
        "in_stock": 9
      }
    }
  ]
}

