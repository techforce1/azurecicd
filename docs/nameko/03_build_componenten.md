## Opdracht 2: Nameko build van benodigde componenten

Nu we de twee base images klaar hebben staan die door de andere Nameko componenten gebruikt worden, lopen we de structuur van het project na om de volgende stappen te bepalen en ook de componenten mee te nemen in een build en naar onze ACR te pushen voor gebruik in een volledige deployment.

Het Nameko project gebruikt een simpele folder structuur met subfolders per component en die structuur willen  we aanhouden bij het samenstellen van de pipeline om een lift-en-shift van het project zoals opgenomen in het originele repo mogelijk te maken, zonder verdere wijzigingen aan folder structuur of source-code.


Elk component heeft z'n eigen folder in het project, zijnde:
- *gateway*: REST API front-end / dispatcher
- *orders*: RPC API endpoint (HTTP -> RPC geproxied via gateway service) voor order processing
- *products*: RPC API endpoint (HTTP -> RPC geproxied via gateway service) voor product info

Check de Dockerfile en Makefile per component folder en voeg aan de pipeline stappen toe om:
- *gateway* te builden + pushen naar jouw ACR
- *orders* te builden + pushen naar jouw ACR
- *products* te builden + pushen naar jouw ACR

Hints: 
- Zet zowel bij het bouwen als bij het pushen de 'include latest tag' optie aan.
- De Dockerfiles gaan uit van non-ACR based image tagging. Pas de Dockerfiles per component folder aan om de eerder gebouwde images uit de Azure ACR te halen met eerder toegekende image tags. De volledige ACR-naam kun je terugvinden in de [Azure Portal](https://portal.azure.com/#home).
- Als in je pipeline stappen fout gaan, hoef je niet elke keer alle stappen opnieuw te doorlopen: je kunt ze per stuk enablen/disablen.
- De Dockerfiles zijn "multi-stage" (meerdere FROM regels). Als je bij het bouwen geen build arguments meegeeft, dan worden alle stages meegenomen.
- Houd de stappen compact en klein per stap en voorkom recursive builds bijvoorbeeld: lastig te debuggen als er iets niet loopt en de wachttijden voor het re-runnen van een stap gaan snel omhoog waarom ook de debug-build dev cycle erg hoog kan worden

