## Opdracht 5: test de volledige applicatie REST endpoints via HTTP

De verschillende componenten zouden nu contact met elkaar moeten hebben binnen dezelfde namespace (examples) op ons AKS cluster. Verifieer dit door:
- http routing te enablen. *let op* dit is niet secure, maar is een snelle manier om functionaliteit te testen en waar nodig k8s config of pipeline staappen bij te sturen zonder extra ingress/https deployemtn stappen te hoeven nemen

Dit doe je middels de volgende stappen:
- enable http routing op het AKS cluster (let op: dit duurt even!) door in de Azure CLI de volgende commando's te runnen:
- az aks create --resource-group [resourcegroup] --name [aksnaam] --enable-addons http_application_routing
- noteer de info die Azure rteruggeeft na aanmaken http routing
- enable HTTP routing in de Azure Portal via resources -> AKS Clusters

Test eerst het product back-end door: 


product aan te maken:
- $ curl -XPOST -d '{"id": "the_odyssey", "title": "The Odyssey", "passenger_capacity": 101, "maximum_speed": 5, "in_stock": 10}' 'http://[[CLUSTER INGRESS IP]/products'

aagemaakt product op te vragen:
$ curl 'http://[[CLUSTERIP]]/products/the_odyssey'

response zou te eerder ingegeven data terug moeten geven in JSON format:

{
  "id": "the_odyssey",
  "title": "The Odyssey",
  "passenger_capacity": 101,
  "maximum_speed": 5,
  "in_stock": 10
}

order aan te maken:
$ curl -XPOST -d '{"order_details": [{"product_id": "the_odyssey", "price": "100000.99", "quantity": 1}]}' 'http://[[CLSUTERIP]]/orders'

order weer terug te vragen:

$ curl 'http://[[CLUSTERIP]]/orders/1'

response zou eerder opgegeven data moeten spiegelen:

{
  "id": 1,
  "order_details": [
    {
      "id": 1,
      "quantity": 1,
      "product_id": "the_odyssey",
      "image": "http://www.example.com/airship/images/the_odyssey.jpg",
      "price": "100000.99",
      "product": {
        "maximum_speed": 5,
        "id": "the_odyssey",
        "title": "The Odyssey",
        "passenger_capacity": 101,
        "in_stock": 9
      }
    }
  ]
}

