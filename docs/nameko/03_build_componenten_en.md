## Assignment 2: Nameko component build 

Now we've got both base images built, pushed, ready and waiting in our ACR, we simply follow the project's structure following both the README.md docs as well as common sense. Every discrete componet has it's own folder and build definitions inside it. We're ready to do a lift-and-shift with a minimal amount of changes to the original repository, source code or build definitions.

Each component has it's own folder inside the project:
- *gateway*: REST API front-end / dispatcher
- *orders*: RPC API endpoint (HTTP -> RPC geproxied via gateway service) voor order processing
- *products*: RPC API endpoint (HTTP -> RPC geproxied via gateway service) voor product info

Check Dockerfile and Makefile for eacht component and add steps to your build pipeline for:
- *gateway* (build & push)
- *orders* (build & push)
- *products* (build & push) 

Hints: 
- Check your tagging options and the 'include latest tag' option depending on your strategy
- The existing Dockerfile definitions assume default docker repo instead of your ACR. Chances are, you'll need to modify the Docker image depencies inside the Dockerfile for each component to match the image you built & pushed to ACR in previous steps.
- Keep things modular: run your pipeline step-by-added-step and only re-run stpes when absolutely necesaary. You can disable each and every step individually before every run.
- Keep in mind: as we're using Azure DevOps pipeline *without* a fixed set of build server(s), any new build step could land on a different Azure DevOps build slave. In this model there's is no guarantee your data will reamin for the next run. So run steps depending on each other in a single pass (build + push in separate steps but in a single run for each image)


