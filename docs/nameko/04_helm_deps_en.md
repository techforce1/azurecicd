## Assignment 3: Install external dependencies through Helm (CLI)

Re-use the existing AKS cluster and:
- Add a new namespace:
  
`kubectl apply -f namespace.yml` OF `kubectl create namespace examples`

- Deploy RabbitMQ:  
`helm  install broker  --namespace examples stable/rabbitmq`

- Deploy PostgreSQL:  
`helm install db --namespace examples stable/postgresql --set postgresDatabase=orders`

- Deploy Redis:  
`helm install cache  --namespace examples stable/redis`

- Check status of all pods:  
`kubectl --namespace examples get po`

