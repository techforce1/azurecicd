## Opdracht 3: Installeer externe dependencies via Helm (CLI)

Hergebruik het AKS cluster v/d Burgerbuilder opdracht en in Cloud Shell:
- Voeg namespace toe met namespace.yml resource file in k8s folder:  
`kubectl apply -f namespace.yml` OF `kubectl create namespace examples`

- Deploy vanaf de Azure CloudShell CLI RabbitMQ dependency met Helm:  
`helm  install broker  --namespace examples stable/rabbitmq`

- Deploy vanaf de Azure CloudShell CLI PostgreSQL database:  
`helm install db --namespace examples stable/postgresql --set postgresDatabase=orders`

- Deploy vanaf de Azure CloudShell CLI Redis:  
`helm install cache  --namespace examples stable/redis`

- Check status van de deployed pods:  
`kubectl --namespace examples get po`



