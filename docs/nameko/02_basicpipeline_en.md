## Assignment 1: Nameko base pipeline

During this Assignment we'll import the source code repository and have a look at the original build process in the project to determine a migration strategy to an Azure DevOps based pipeline

- Logon to Azure DevOps with your current credentials
- Import the repo at https://gitlab.com/techforce1/nameko-azuredevops.git
     - Choose "Repos" on the left-hand menu
     - Top op page breadcrumbs -> "Import Repository" 
     - Provide type Git & URL hierboven and wait for import.
  
- Next step is creating a first basic build pipeline from the imported repository. The repo's existing build is mostly Makefile & Docker file driven.  As a first start, browse through the README.md and following that all Makefile and accompanying Dockerfiles in the given (sub)folders. The Docker builds are layered and depending on each other. The base for all component builds is defined in the Dockerfile and Makefile definitions in the root of the repo. There's a *lot* of different ways to create a solution, but for our example we'll use the classic pipeline and steps to translate the build and deployment.
    
- Assignment 1: create an Azure DevOps build pipeline that builds and pushes the first base image to your ACR
  
  Read the Makefiles and Dockerfiles in the root folder and subfolders and determine how to build the first two base images used throughout this project: nameko-example-base and nameko-example-builder

  - "Pipelines" menu -> New Pipeline
  - Choose the Classic Editor if you want to follow our demonstrated examples, YAML is fine as well but due to all service principles and auth related variables not the cleanest and clearest way of demonstrating on stage. Functionally all blocks are the same. Use the nameko-azuredevops, branche master 
    - Choose the Docker Container template as starting point and keep a few base CI/CD principles in mind:
      - build your pipeline based on clearly defined simple steps per image and goal. 
      - you could try to do everything recursively in one huge build definition but the turnaround time on a failure and debug cycle will make things harder
      - Divide the work over simple basic steps, build, push, etc. You can enable/disable every step in the pipeline following success or failure, the more modular the quicker the conversion.
    - So go lock-step:
     - Build a base image
     - Push a base image
     - Check your results inside the ACR 

At the end of this exercise you'll have built a pipeline that:
- builds nameko-example-base
- pushes nameko-example-base
- builds nameko-example-builder
- pushes nameko-example-builder