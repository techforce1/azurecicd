## Nameko 
**Python based microservices**

Based on an example project of the Nameko Python-based microservice framework we'll build and deploy a slightly more complex project on the previously provisioned AKS / ACR resources. 

### Goals


- Set-up a pipeline for multiple microservices & components
- Convert an existing Docker/make/k8s based build to an Azure DevOps configuration
- Deploy al built components to your AKS
- Test the entire service chain using basic REST calls

Handy URLs for this session:
- [Techforce1 Nameko repo](https://gitlab.com/techforce1/nameko-azuredevops.git)
- [Azure DevOps portal](https://dev.azure.com)
- [Azure portal](https://portal.azure.com/#home)
- [Azure cloudshell info](https://gitlab.com/techforce1/azurecicd/blob/master/docs/cloudshell.md)