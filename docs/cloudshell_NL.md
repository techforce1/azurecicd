## Azure en Cloudshell

### Azure interfaces
Er zijn meerdere interfaces om met Azure te communiceren.  
Naast de webinterface kun je o.a. gebruik maken van de CLI. Daarnaast hebben alle hippe en minder hippe programmeertalen libraries die tegen de API aan kunnen praten. En vergeet de configuration management tools niet, zoals Ansible en Terraform.  
Verder gaan we in de workshop communiceren met een Kubernetes cluster in Azure, waarvoor we gebruik maken van Kubernetes CLI.

De kans bestaat dat je alle benodigde tooling in een verse variant op je laptop hebt staan. Het gaat hierbij in ieder geval om:
- Azure cli (commando: `az`)
- Kubernetes cli (commando: `kubectl`)
- Helm Kubernetes package manager (commando: `helm`)  

Je **mag** je eigen tooling gebruiken, maar dat raden we voor deze workshop af. Via Cloudshell is namelijk allerlei authenticatie al geregeld, en dat voorkomt een hoop geknutsel die nauwelijks relevant is voor deze workshop.  

### Wat is cloudshell?
Als je deze zaken (*az*, *kubectl*, *helm*) nog niet geconfigureerd hebt op je laptop, dan kun je gebruik maken van Cloudshell. Dit is een shell (*bash* of *powershell*, naar keuze) die via een webbrowser te bereiken is en waarin alle genoemde tooling (en meer) vooraf geconfigureerd is, zodat het ook gekoppeld is aan je Azure account.  
Onder water maakt Cloudshell gebruik van een stukje storage in je Azure account, zodat wat je in de Cloudshell uitvoert ook voor het nageslacht bewaard kan blijven (zolang je het in je homedirectory hebt staan).  
Voor meer info over Cloudshell, zie [documentatie](https://docs.microsoft.com/nl-nl/azure/cloud-shell/overview).

### Mogelijkheden van Cloudshell
- editors (vim, nano, code, emacs)
- azure cli
- git
- container-tools (kubectl, helm)
- configmanagement/orchestration (ansible, terraform, puppet)
- databaseclients (MySQL, PostgreSQL)

Zie [documentatie](https://docs.microsoft.com/nl-nl/azure/cloud-shell/features) voor nog meer mogelijkheden.

### Hoe start je Cloudshell?
Stuur je browser naar https://shell.azure.com. Als je die al eens eerder hebt gestart dan kom je in je bestaande cloudshell terecht. Als dit de eerste keer is, dan wordt er een verse voor je aangemaakt.    
Of klik op het Cloudshell-icoon in de [Azure portal](https://portal.azure.com).