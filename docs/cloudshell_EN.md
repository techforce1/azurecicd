## Azure and Cloudshell

### Azure interfaces
Multiple interfaces to communicate with Azure exist.  
Besides the webinterface, there is a CLI (Command Line Interface). Almost every programming language contains libraries to access the Azure API.  Also configuration management tooling like *Ansible* and *Terraform* support Azure.  
In this assignment we will build an Azure Kubernetes cluster using *kubectl* (Kubernetes CLI) and *helm* (Kubernetes package manager).  

It's possible you already have the tooling installed on your laptop. We will use:
- Azure cli (command: `az`)
- Kubernetes cli (command: `kubectl`)
- Helm Kubernetes package manager (command: `helm`)  

If you really want to, you can use your own versions. But we do not support it in this workshop. Cloudshell already added authentication for accessing Azure API's, so it's much easier to use Cloudshell.  

### Cloudshell?
It's a shell (*bash* of *powershell*) which can be accessed by a webbrowser. It's loaded with tools like editors and Command Line Interfaces.  It's connected to your Azure account.  
It uses a piece of storage in Azure which is your *homedirectory*. Everything in your homedirectory and your command-line-history is accessible in other Cloudshell sessions, even if you close the original session.  
More information about Cloudshell: [documentation](https://docs.microsoft.com/en-us/azure/cloud-shell/overview).

### Cloudshell features
- editors (vim, nano, code, emacs)
- azure cli
- git
- container-tooling (kubectl, helm)
- configmanagement/orchestration (ansible, terraform, puppet)
- databaseclients (MySQL, PostgreSQL)

### How to start Cloudshell?
Point your browser to https://shell.azure.com. When you're not a first-timer, an existing cloudshell will be opened. You can also click on the Cloudshell-icon in the [Azure portal](https://portal.azure.com).