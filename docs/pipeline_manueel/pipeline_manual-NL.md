## Aanmaken van een Linux container

Login op [​https://shell.azure.com](%E2%80%8Bhttps://shell.azure.com) en kies: **Bash**.
Of gebruik de [Azure CLI voor Linux](https://docs.microsoft.com/nl-nl/cli/azure/install-azure-cli-linux?viewa=azure-cli-latest).

Vervang bij onderstaande commando's de string ​ **tf1helloworld** ​ door een eigen unieke string voor de location c.q de aangegeven locatie waarin je werkt. Bijvoorbeeld je initialen.
Gebruik echter geen speciale karakters  zoals _$! etc. in je string.

Dit gaat het makkelijkst als je beide als shell variabelen definieert.

```
$ YOURSTRING="tf1helloworld"
$ LOCATION="westeurope"
```


### Aanmaken [resource groep](https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/manage-resource-groups-portal)

```
az group create --location $LOCATION --name $YOURSTRING-rg
```
### Aanmaken van een [app service plan](https://docs.microsoft.com/nl-nl/azure/app-service/overview-hosting-plans), type Linux

```
az appservice plan create -g $YOURSTRING-rg -n $YOURSTRING-service-plan --is-linux
```

### Aanmaken van een [App Service](https://docs.microsoft.com/nl-nl/azure/app-service/)
Vanuit de gemaakte app service plan met **Tomcat en JRE 8 als** runtime.
```
az webapp create -g $YOURSTRING-rg -p $YOURSTRING-service-plan -n $YOURSTRING --runtime "TOMCAT|8.5-jre8"
```
Of combineer al de bovenstaande commando in de onderstaande onliner:
```
az group create --location $LOCATION --name $YOURSTRING-rg \
&& az appservice plan create -g $YOURSTRING-rg -n $YOURSTRING-service-plan --is-linux \
&& az webapp create -g $YOURSTRING-rg -p $YOURSTRING-service-plan -n $YOURSTRING --runtime "TOMCAT|8.5-jre8"
```
Dit alles creëert een op Docker gebaseerde Linux container, die bereikbaar is op:

[https://tf1helloworld.azurewebsites.net](https://tf1helloworld.azurewebsites.net)


## Importeren Git repo in Azure DevOps

 - Login op : ​[https://dev.azure.com](https://dev.azure.com)
 - Kies rechtsboven in bij ***Profile settings*** ( poppetje icoon) > ***Preview features***
en zet de volgende optie uit: ​ ***New Repos landing pages***
 - Selecteer nu  ***New Project*** > ***Create new project***
 -  Vul in bij ***Project name*** : ​ _**tf1helloworld_**
en kies ***Private > Create Project***.

Nu wordt je Azure DevOps project aangemaakt en zal **Welcome to the project** tekst in beeld krijgen.

 - Kies  ***Repos > Import***
 - Source type : ***Git***
 - Vul in  by Clone URL : https://gitlab.com/techforce1/helloworld.git
 - Selecteer vervolgens: ***Import***


## Aanmaken Build en Deploy Pipeline

Nu de externe git repo geimporteerd is gaan we een  build en deploy pipeline aanmaken.

 - Kies nu in Azure Devops ***Pipelines > Create Pipeline***
 - Selecteer ***Azure Repos Git*** > ​ en project : *tf1helloworld*
 - Bij ***Configure your pipeline*** selecteer : ***Maven package Java project Web App to Linux onAzure.***
 - Kies bij ***Select Azure subscription*** : *Azure-abonnement Techforce1* > **Continue**
 - Bij  ***Web App name*** selecteer dan  *tf1helloworld* en klik op ***Validate and configure.***

Nu in het scherm krijg je de melding : Review your pipeline YAML.
De azure-pipelines.yml is de pipeline definitie die bestaat uit variabelen te samen met stages , build en deploy.

Maken maven package met artficact : helloworld-1.0-SNAPSHOT.war
en Deploy stage naar de eerder gemaakt webapp resource.

 - Kies ***Save and Run*** en nogmaals ***Save and Run***

Bekijk nu de uitvoering van de ***build stage*** en vervolgens de ***deploy stage***.


## Resultaat

Je applicatie code zou nu bereikbaar moeten zijn op:
https://< unieke string  >.azurewebsites.net/helloworld-1.0-SNAPSHOT

Ofwel in dit voorbeeld: [https://tf1helloworld.azurewebsites.net/helloworld-1.0-SNAPSHOT](https://tf1helloworld.azurewebsites.net/helloworld-1.0-SNAPSHOT)

Troubleshoot eventueel via :
[https://tf1helloworld.scm.azurewebsites.net](https://tf1helloworld.scm.azurewebsites.net)

# Code aanpassen in Azure repo

Zodra je de een bestand wijzigt in je Azure git repo en de wijziging commit zal je de pipeline opnieuw triggeren.

 - Ga naar Repos in [​https://dev.azure.com](https://dev.azure.com)​ naar je project​ ***tf1helloworld*** .
 - Wijzig ***index.jsp*** en vervang:
<h2>Hello World!</h2>
naar bijvoorbeeld:
<h2>Hi Geeks!</h2>

 - Selecteer ***Commit***.

Nu met de commit trigger je opnieuw de pipeline en wordt er een nieuwe build en deployment gemaakt.

## Opschonen resource groep c.q Linux container

Het opruimen van de Linux container met deployment erin gaat met het volgende Azure shell met commando,  wat de Azure resource group in zijn geheel verwijderd.
```
az group delete --name $YOURSTRING-rg
```
Ofwel in dit voorbeeld:

```
az group delete --name tf1helloworld-rg
```
ltoning@DESKTOP-G0QQVCK:~/azurecicd/docs/pipeline_manueel$





















