## Create a Linux container

Login at [​https://shell.azure.com](%E2%80%8Bhttps://shell.azure.com) and select: **Bash**. 
Or use [Azure CLI for Linux](https://docs.microsoft.com/nl-nl/cli/azure/install-azure-cli-linux?view=azure-cli-latest).

For the following commands replace  the string ​ **tf1helloworld** ​ with your own unique string:

### Create a [resource groep](https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/manage-resource-groups-portal)

```
az group create --location westeurope --name tf1helloworld-rg
```
### Create an [app service plan](https://docs.microsoft.com/nl-nl/azure/app-service/overview-hosting-plans), type Linux

```
az appservice plan create -g tf1helloworld-rg -n tf1helloworld-service-plan --is-linux
```

### Creation of an [App Service](https://docs.microsoft.com/nl-nl/azure/app-service/) 
We create an application service from the former created app service plan with **Tomcat en JRE 8 **  runtime.
```
az webapp create -g tf1helloworld-rg -p tf1helloworld-service-plan -n tf1helloworld --runtime "TOMCAT|8.5-jre8"
```
Or combine all the above commands with the following onliner:
```
az group create --location westeurope --name tf1helloworld-rg \
&& az appservice plan create -g tf1helloworld-rg -n tf1helloworld-service-plan --is-linux \
&& az webapp create -g tf1helloworld-rg -p tf1helloworld-service-plan -n tf1helloworld --runtime "TOMCAT|8.5-jre8"
```
This will create a Docker based Linux container, that will be reachable at:

[https://tf1helloworld.azurewebsites.net](https://tf1helloworld.azurewebsites.net)


## Import Git repo in Azure DevOps

 - Login in at : ​[https://dev.azure.com](https://dev.azure.com)
 - Select at the richt top at ***Profile settings*** ( user icon) > ***Preview features***
and deselect the following option: ​ ***New Repos landing pages***
 - Select  ***New Project*** > ***Create new project***
 -  Fill in at ***Project name*** : ​ _**tf1helloworld_**
and select ***Private > Create Project***.

At this point your Azure DevOps project will be  created and you should get the message **Welcome to the project** on screen.

 - Select ***Repos > Import***
 - Source type : ***Git*** 
 -Fill in at by Clone URL : https://gitlab.com/techforce1/helloworld.git
 - Select: ***Import***


## Create a Build en Deploy Pipeline

As the external git repo  is now imported we will proceed and create a  build en deploy pipeline.

 - Select Azure Devops ***Pipelines > Create Pipeline***
 - Select ***Azure Repos Git*** > ​ and  project : *tf1helloworld*
 - At ***Configure your pipeline*** select : ***Maven package Java project Web App to Linux onAzure.***
 - Select at  ***Select Azure subscription*** : *Azure-abonnement Techforce1* > **Continue**
 - At ***Web App name*** select  *tf1helloworld* en click on ***Validate and configure.***

At this point you will get the following message on screen : **Review your pipeline YAML.**
The azure-pipelines.yml  is the pipeline definition existing out of variables and the main stages , build en deploy. 

It will Build create a maven package with the artficact : helloworld-1.0-SNAPSHOT.war
and a  Deploy stage pointing to former created webapp resource.

 - Select ***Save and Run*** and once again select ***Save and Run***  

Review at this point the execution of the  ***build stage*** and the upfollowing ***deploy stage***.


## Result

Your application code should now be reachable at:
https://< your unique string  >.azurewebsites.net/helloworld-1.0-SNAPSHOT

In this specific example at: [https://tf1helloworld.azurewebsites.net/helloworld-1.0-SNAPSHOT](https://tf1helloworld.azurewebsites.net/helloworld-1.0-SNAPSHOT)

Troubleshoot if needed via :
[https://tf1helloworld.scm.azurewebsites.net](https://tf1helloworld.scm.azurewebsites.net)

# Code adjustments in Azure repo

As soon as file within the  Azure git repo  changes with a commit the pipeline will be triggered.

 - Go to Repos at [​https://dev.azure.com](https://dev.azure.com)​  in your project​ ***tf1helloworld*** .
 - Change***index.jsp*** and replace:
<h2>Hello World!</h2>
to for example:
<h2>Hi Geeks!</h2>

 - Select ***Commit***.

This will trigger the pipeline and so a new  build and deployment will be created.

## Cleaning up the resource groep c.q Linux container

To cleanup whole Linux container with the deployment in it use the following Azure shell command,  whichh will  remove the entire Azure resource group.
```
az group delete --name < your unique string >-rg
```
Or specifiek for this example:

```
az group delete --name tf1helloworld-rg
```
