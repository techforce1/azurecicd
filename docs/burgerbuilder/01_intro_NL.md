## Burgerbuilder
**Of: hoe bouw ik een hamburger?**

Nu je een beetje weet hoe pipelines in Azure verwerkt zijn, gaan we zelf wat bouwen. 

Doel: 
- een container bouwen en deployen op een kubernetes cluster in Azure;
- waarin  een applicatie draait die op een veilige manier bereikbaar is via een publieke webinterface;
- waarin je met een webbrowser een hamburger kunt samenstellen.  

Uiteraard gaan we het bouwen en deployen automatiseren, zodat dit aangetrapt wordt bij een wijziging in de code.

Het Azure Kubernetes Service cluster dat in deze opdracht wordt gemaakt, wordt ook gebruikt bij de volgende (Nameko) opdracht.

Omdat we allemaal met een user werken die onder hetzelfde Azure account valt, kunnen we elkaar potentieel in de wielen rijden. Laten we dat niet doen 😏
Om die reden heb je een eigen 'locatie' (region) gekregen, want er zitten beperkingen aan het aantal resources dat je binnen een region/locatie kunt gebruiken. Maak dus uitsluitend resources aan in je eigen locatie.

Gegevens die je nodig hebt bij deze opdracht (links):
- [Gitlab repo burgerbuilder](https://gitlab.com/techforce1/burger-builder.git)
- [Azure DevOps portal](https://dev.azure.com)
- [Azure portal](https://portal.azure.com/#home)
- [Azure cloudshell](https://shell.azure.com)
- [Azure cloudshell howto](https://gitlab.com/techforce1/azurecicd/blob/master/docs/cloudshell.md)

## Inloggen in Azure
- Het account waarmee je inlogt is de gegeven gebruikersnaam met direct daaraan vast *@infotechforce1.onmicrosoft.com*
- Stuur je browser naar [Azure portal](https://portal.azure.com/#home), je moet nu eerst je wachtwoord wijzigen
- Stuur je browser (in een andere tab/browser) naar [CloudShell](https://shell.azure.com)
  - Kies voor **bash**
  - Je ziet nu in welke *subscription* je werkt, kies voor **Create Storage**
- Stuur je browser (in een andere tab/browser) naar [Azure DevOps](https://dev.azure.com)
  - Je hebt al een account, dus klik op het linkje daarvoor
  - Klik door in de login-wizard (eenmalig)

### Voor de gevorderden
In *burgerbuilder_gevorderden.md* staat de workshop als verkort stappenplan. Hierin wordt alleen de essentiële informatie gegeven en moet je zelf de puntjes met elkaar verbinden.

### Als je er niet uitkomt
Onder de namen *02_kubcluster_uitwerking.md*, *03_pipeline_uitwerking.md*, *04_secure_uitwerking.md* kun je de uitwerkingen vinden.

### Quickies
Daarnaast zijn wat shell-scripts gemaakt die uitvoeren wat je normaal gesproken handmatig zou doen in deze workshop. Hiermee kun je sneller door de onderdelen heen lopen, bijvoorbeeld wanneer je vastloopt en helemaal van voor af aan wil beginnen.
Je kunt ze vinden in deze repository in de directory *scripts*.
Per onderdeel wordt aangegeven welk script gebruikt kan worden.
Om ze te kunnen gebruiken clone je deze git-repository vanuit je Cloudshell en ga je naar de directory *azurecicd/scripts/burgerbuilder*
```
git clone https://gitlab.com/techforce1/azurecicd.git
cd azurecicd/scripts/burgerbuilder
```
