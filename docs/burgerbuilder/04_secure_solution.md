## 4. Secure the application

The application is now running at port TCP/3000 and uses plain-text communication. You must enhance security by adding SSL (https). You will create an Nginx reverse proxy which will be responsible for SSL-offloading.

Tip: You can start multiple Cloudshell sessions just by pointing another tab/browser to https://shell.azure.com.

---------------------

[Helm](https://helm.sh/), kind of a package manager for Kubernetes, will be used to deploy an Nginx proxy with an ingress controller. The `helm` CLI can deploy helm-charts (predefined Kubernetes packages) to a Kubernetes cluster.  
The next step is to configure Nginx to offload the SSL-traphic and to connect to port TCP/3000 of the application container in the Kubernetes cluster.

- Create file *frontend-service.yml*
```
# filename: frontend-service.yml

apiVersion: v1
kind: Service
metadata:
  name: frontend
spec:
  selector:
    app: frontend
  ports:
  - protocol: TCP
    port: 80
    targetPort: 3000
  type: ClusterIP
```

- Use *kubectl* to configure the frontend service. It will create a service in Kubernetes which accepts traphic on port TCP/80 and forwards the traphic to port TCP/3000.  
** Note**: use the correct namespace (same namespace you used to create the pipeline, by default it's *default*).  
`kubectl apply -f frontend-service.yml -n namespace`

- Add some helmchart-repositories to *helm*.  
`helm repo add stable https://kubernetes-charts.storage.googleapis.com/`  
`helm repo add jetstack https://charts.jetstack.io`  
`helm repo update`  


- Create an Nginx ingress controller. Helm 3.0 will not automatically create a namespace (previous versions did), so we must create one ourselves.  
`kubectl create namespace ingress`  
`helm upgrade --install ingress stable/nginx-ingress --namespace ingress`

- Retrieve the public IP address of the ingress controller (*external ip*).  
`kubectl get svc -n ingress ingress-nginx-ingress-controller -o jsonpath="{.status.loadBalancer.ingress[*].ip}"`


- Display the services, you will need them later on.  
**Note**: use the correct namespace.  
`kubectl get services --namespace namespace`

- Create the file *frontend-ingress.yml*.  
**Note**: change *_INGRESS_CONTROLLER_EXTERNAL_IP_* and *serviceName*  
```
# filename: frontend-ingress.yml

apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: frontend
spec:
  rules:
  - host: frontend._INGRESS_CONTROLLER_EXTERNAL_IP_.nip.io
    http:
      paths:
      - backend:
          serviceName: burgerbuilder
          servicePort: 3000
        path: /
```

- Create the ingress resource.  
**Note**: use the correct namespace.  
`kubectl apply -f frontend-ingress.yml -n namespace`

- Install package *cert-manager* in the Kubernetes cluster.  
**Note**: use the correct namespace.  
`kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.12/deploy/manifests/00-crds.yaml -n default`  
`helm install cert-manager jetstack/cert-manager --set ingressShim.defaultIssuerName=letsencrypt --set ingressShim.defaultIssuerKind=ClusterIssuer --version v0.12.0`  

- Create file *clusterissuer.yml*.  
```
# filename: clusterissuer.yml

apiVersion: cert-manager.io/v1alpha2
kind: ClusterIssuer
metadata:
  name: letsencrypt
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory # production
    email: test@mail.com # replace this with your email
    privateKeySecretRef:
      name: letsencrypt
    # Enable the HTTP-01 challenge provider
    solvers:
    - http01:
        ingress:
          class: nginx
```

- Create the issuer.  
**Note**: use the correct namespace.  
`kubectl apply -f clusterissuer.yml -n namespace`

- Create file *frontend-ingress-tls.yml*.  
**Note**: change *_INGRESS_CONTROLLER_EXTERNAL_IP_* to the correct external IP address.  
```
# filename: frontend-ingress-tls.yml

apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: frontend
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt
spec:
  tls:
  - hosts:
    - frontend._INGRESS_CONTROLLER_EXTERNAL_IP_.nip.io
    secretName: frontend-tls-secret
  rules:
  - host: frontend._INGRESS_CONTROLLER_EXTERNAL_IP_.nip.io
    http:
      paths:
      - backend:
          serviceName: burgerbuilder
          servicePort: 3000
        path: /
```

- Update ingress resource.  
`kubectl apply -f frontend-ingress-tls.yml -n namespace`  

Now it's time to order a safe burger through https://frontend.A.B.C.D.nip.io/burger (*A.B.C.D* is the external IP address).

### Fast-lane
The script `configure_ssl.sh` requires you to enter some data:  
- Servicename of the container in the AKS cluster (name of the pipeline, check *services* in the Kubernetes Dashboard).  
- Port-number of the application you entered in the pipeline (probably *3000*).  
- Kubernetes namespace containing the service (burgerbuilder container).  You entered this when you assembled the pipeline, you can check it in the Kubernetes Dashboard.  

```
user@Azure:~/azurecicd/scripts/burgerbuilder$ ./configure_ssl.sh
Enter service name in aks-cluster: burgerbuilder
Enter port number for that service: 3000
Enter kubernetes namespace for that service: default

Continue? (Y/N) y

Deploy frontend service ... OK
Configure helm-chart repositories ... OK
Deploy nginx ingress controller ... OK
Wait for 30 seconds, to let it settle ... 
Retrieve external IP-address ... OK (52.139.155.61)
Create ingress resource ... OK
Install certificate manager prerequisites... OK
Install certificate manager ... OK
Wait for a minute to let it settle ... OK
Issue certificate ... OK
Create ingress TLS resource ... OK

Browse to https://frontend.52.139.155.61.nip.io
```

The parts of the script fail, you can check logging (*configure_ssl.log*) and/or restart the script. Succesfully executed steps will be skipped.  
