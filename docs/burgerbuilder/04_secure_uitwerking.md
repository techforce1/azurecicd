## 4. Maak het veilig

De applicatie luistert op poort 3000, en praat plain-text html. Dat is natuurlijk niet veilig genoeg voor deze bijzondere applicatie. Daarom gaan we security opschroeven en dat doen we door er een nginx reverse proxy voor te zetten. Deze maakt gebruik van een SSL-certificaat, waardoor de burgerbuilder netjes SSL lijkt te praten.
  
Tip: Mocht je een 2e cloudshell sessie nodig hebben (en dat heb je), dan kun je eenvoudig nog een browsersessie starten naar https://shell.azure.com.

---------------------

Met behulp van [Helm](https://helm.sh/) gaan we ervoor zorgen dat een Nginx proxy met ingress controller wordt opgespind. Met het `helm` commando kun je helm-charts (voorgedefinieerde Kubernetes packages) voeren aan een Kubernetes cluster.  
Daarna gaan we Nginx configureren zodat het SSL-verkeer getermineerd wordt, en daarna verbinding gemaakt wordt met poort TCP/3000 van de applicatie-container.

- Maak het bestand *frontend-service.yml* aan.
```
# filename: frontend-service.yml

apiVersion: v1
kind: Service
metadata:
  name: frontend
spec:
  selector:
    app: frontend
  ports:
  - protocol: TCP
    port: 80
    targetPort: 3000
  type: ClusterIP
```

- Voer dit bestand aan *kubectl*. Hiermee wordt een service aangemaakt die het verkeer op poort 80 opvangt en doorstuurt naar poort 3000.  
**Let op**: gebruik de juiste namespace (dezelfde als je gebruikt hebt in de pipeline, het voorbeeld gebruikte *default*).  
`kubectl apply -f frontend-service.yml -n namespace`

- Voeg je helmchart-repositories toe die we gaan gebruiken.  
`helm repo add stable https://kubernetes-charts.storage.googleapis.com/`  
`helm repo add jetstack https://charts.jetstack.io`  
`helm repo update`  

- Installeer de Nginx ingress controller in het cluster. Helm 3.0 maakt niet automatisch een namespace meer aan, voorgaande versies wel, dus dat moeten we eerst even zelf doen.  
`kubectl create namespace ingress`  
`helm upgrade --install ingress stable/nginx-ingress --namespace ingress`

- Haal het publieke IP-adres van de ingress controller op (external ip).  
`kubectl get svc -n ingress ingress-nginx-ingress-controller -o jsonpath="{.status.loadBalancer.ingress[*].ip}"`

- Vraag de services op, die heb je hierna nodig.  
**Let op**: gebruik de juiste namespace (dezelfde als je gebruikt hebt in de pipeline).  
`kubectl get services --namespace namespace`

- Maak het bestand *frontend-ingress.yml* aan (let op *_INGRESS_CONTROLLER_EXTERNAL_IP_* en *serviceName*).
```
# filename: frontend-ingress.yml

apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: frontend
spec:
  rules:
  - host: frontend._INGRESS_CONTROLLER_EXTERNAL_IP_.nip.io
    http:
      paths:
      - backend:
          serviceName: burgerbuilder
          servicePort: 3000
        path: /
```

- Maak de ingress resource aan.  
**Let op**: gebruik de juiste namespace (dezelfde als je gebruikt hebt in de pipeline).   
`kubectl apply -f frontend-ingress.yml -n namespace`

- Installeer de package cert-manager in het cluster (let op de namespace).  
`kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.12/deploy/manifests/00-crds.yaml -n default`  
`helm install cert-manager jetstack/cert-manager --set ingressShim.defaultIssuerName=letsencrypt --set ingressShim.defaultIssuerKind=ClusterIssuer --version v0.12.0`  

- Maak het bestand *clusterissuer.yml* aan
```
# filename: clusterissuer.yml

apiVersion: cert-manager.io/v1alpha2
kind: ClusterIssuer
metadata:
  name: letsencrypt
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory # production
    email: test@mail.com # replace this with your email
    privateKeySecretRef:
      name: letsencrypt
    # Enable the HTTP-01 challenge provider
    solvers:
    - http01:
        ingress:
          class: nginx
```

- Maak issuer aan (let op de namespace).  
`kubectl apply -f clusterissuer.yml -n namespace`

- Maak het bestand *frontend-ingress-tls.yml* aan.  
Let op: gebruik het juiste IP-adres voor *_INGRESS_CONTROLLER_EXTERNAL_IP_*
```
# filename: frontend-ingress-tls.yml

apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: frontend
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt
spec:
  tls:
  - hosts:
    - frontend._INGRESS_CONTROLLER_EXTERNAL_IP_.nip.io
    secretName: frontend-tls-secret
  rules:
  - host: frontend._INGRESS_CONTROLLER_EXTERNAL_IP_.nip.io
    http:
      paths:
      - backend:
          serviceName: burgerbuilder
          servicePort: 3000
        path: /
```

- Update ingress resource (let op de namespace).  
`kubectl apply -f frontend-ingress-tls.yml -n namespace`  

Je kunt nu burgers gaan bestellen op een veilige manier via https://frontend.A.B.C.D.nip.io/burger, waarbij A.B.C.D het externe IP-adres is.

### Als je er niet uitkomt
Bij het script `configure_ssl.sh` moet je een aantal gegevens invoeren:  
- De servicenaam van de container in het AKS cluster (naam van de pipeline, kijk anders bij *services* in het Kubernetes Dashboard).  
- Het poortnummer waarop die service luisters (heb je opgegeven in de pipeline, waarschijnlijk 3000).  
- De Kubernetes namespace waarin de service draait (heb je opgegeven in de pipeline, kijk anders in het Kubernetes Dashboard).  

```
cursist@Azure:~/azurecicd/scripts/burgerbuilder$ ./configure_ssl.sh
Enter service name in aks-cluster: burgerbuilder
Enter port number for that service: 3000
Enter kubernetes namespace for that service: default

Continue? (Y/N) y

Deploy frontend service ... OK
Configure helm-chart repositories ... OK
Deploy nginx ingress controller ... OK
Wait for 30 seconds, to let it settle ... 
Retrieve external IP-address ... OK (52.139.155.61)
Create ingress resource ... OK
Install certificate manager prerequisites... OK
Install certificate manager ... OK
Wait for a minute to let it settle ... OK
Issue certificate ... OK
Create ingress TLS resource ... OK

Browse to https://frontend.52.139.155.61.nip.io
```

Wanneer er iets mis gaat, kun je de logging bekijken (*configure_ssl.log*) en/of het script opnieuw starten. Stappen die al succesvol uit zijn gevoerd worden dan overgeslagen.  
