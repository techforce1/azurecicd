## 3. Creating the pipeline
- Create a new project in [Azure DevOps portal](https://dev.azure.com). Assign the project a custom name (*burgerbuilder* is fine name, and fits perfectly).
  - To be able to deploy successfully to the Kubernetes cluster, you must - prior to creating a pipeline - enable a *preview feature*. You can find the preview feature top-right.
  - You should also disable one of the preview features: ***New Repos landing pages***. When you don't, a *reactjs* error will be thrown while importing a git repository (which you will do later on).  

- Import the [burgerbuilder](https://gitlab.com/techforce1/burger-builder.git) repository from Gitlab.  

- Create an *Azure repos git* pipeline.  
  - You just imported one, select that one.  
  - You will deploy to an AKS cluster.
    - Select your subscription (there is just one available).
    - Select your own AKS cluster and your own registory. The application listens to port 3000. Use the ***default*** namespace.  
    - Validate the configuration.  
  - A *yaml* will be generated, which will be pushed to hit. Click at **save and run** to accept.

- The first build will be created, it take about 3 to 4 minutes. When it succeeds, the container (artifact) will be deployed to the Kubernetes cluster, which will take about 1 minute.  
When you issue `kubectl get pods` in the *cloudshell*, you can notice the new running container.  
When you access the Kubernetes dashboard, the container will also be displayed.  

When the container is running on the AKS cluster, several ways are available to show how to access it with the browser:
- Access the Azure portal. Select ***all resources***. You will not just see your own stuff, but also the resources created by other workshoppers. Therefor it's prefered to group your resources together. 
  - Find the *load balancer* and check which public IP addresses are assigned. One of them is the service you just created, you can recognize it to the fact it contains a *rule*. The IP-address of that service is accessible at port *TCP/3000* with a browser.  
- Access the Kubernetes dashboard, go to *services* (under *Discovery and Load Balancing*. Of course it's just accessible when you selected the correct *namespace*.  
  - The *External Endpoint* of the *burgerbuilder* service is the public IP address (and port) where the service is accessible from the internet.  
  
When you successfully completed the steps above, you are able to create a unsafe burger. 

### When it doesn't work as expected
Unfortunately there are no scripts available, because everything is create using a browser.  