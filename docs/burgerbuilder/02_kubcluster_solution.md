## 2. Kubernetes cluster
First step is to create a Kubernetes cluster. That will be our deployment target. This cluster must be created in your *region*.
You can name the cluster with a custom name. To make that happen, issue the following commands in the cloudshell. This example uses the prefix *tf1ws* and the region *eastasia*. Your settings will be different.

1. Create the resource-group.  
`az group create --name tf1wsgroup --location eastasia`

2. Which kubernetes versions are supported in this region?  
`az aks get-versions -l  eastasia --query 'orchestrators[].orchestratorVersion' -o tsv`
  
3. Select the most recent 1.15.x version. Higher versions (1.16.0+) contain some changes to the API which are not (yet) supported by Azure Pipelines.  
`version=1.15.7`  

4. Create the Azure Container Registry. Use SKU-type *Basic* (SKU is about performance and size).    
`az acr create --resource-group tf1wsgroup --name tf1wsacr --sku Basic`

5. Create the kubernetes cluster, it will take about 5 minutes.    
`az aks create --resource-group tf1wsgroup --kubernetes-version $version --name tf1wsaks --node-count 2 --generate-ssh-keys --attach-acr tf1wsacr`

6. Retrieve the kubernetes credentials which will be used later on by *kubectl*.    
`az aks get-credentials --resource-group tf1wsgroup --name tf1wsaks`

7. To manage the Kubernetes cluster - where the containers will run - more easily, you will make a Kubernetes Dashboard available.  
`kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard`  
`az aks browse --name tf1wsaks --resource-group tf1wsgroup`  

The previous command will diplay an URL, point your browser to that URL to access the Kubernetes dashboard.  

### When things don't work out  
The following example creates a Kubernetes cluster in region *eastasia*. All resource will be prefixed by *tf1ws*. This is just an example, use your own *region* and *prefix*.  
```
cursist@Azure:~/azurecicd/scripts/burgerbuilder$ ./create_aks.sh

Enter location (enter to list locations): eastasia

Enter prefix for resources to be created: tf1ws

Creating resource group tf1wsgroup in location eastasia ... OK
Creating Azure Container Registry tf1wsacr ... OK
Creating Kubernetes cluster tf1wsaks (this may take a while) ... OK
Retrieving Kubernetes credentials ... OK
Configuring access to Kubernetes dashboard ... OK

Execute the following command to get access to the Kubernetes dashboard:
az aks browse --name "tf1wsaks" --resource-group "tf1wsgroup"
```

Execute the command to access the Kubernetes dashboard.