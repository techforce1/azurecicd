# Burgers voor gevorderden
Voor alle aan te maken resources geldt:
- Maak ze uitsluitend aan in je eigen locatie (region).
- Voorzie ze van een zelfbedacht prefix zodat je ze makkelijk kunt herkennen (bijvoorbeeld: *bla01*).

### Azure Kubernetes Service cluster maken
Uitwerking: `02_kubcluster_uitwerking.md`

Onderstaande commando's kunnen allemaal met de Azure CLI (commando *az*) worden uitgevoerd.
1. Maak een resourcegroup aan. De resources die je hierna aanmaakt moeten landen in die resourcegroup.
2. Achterhaal wat de meest recente Kubernetes versie is die op jouw locatie wordt aangeboden door Azure. Dit is namelijk niet voor alle locaties gelijk.
3. Maak een container registry (*ACR*) aan met de *service tier* (SKU) ***Basic***.
4. Bouw een AKS cluster met hoogst ondersteunde Kubernetes 1.15.x versie op jouw locatie. Versie 1.16.x en hoger hebben wijziging in de API die (nog) niet ondersteund worden door Azure Pipelines. Het *node-count* van dit cluster is 2. Koppel het cluster aan de ACR die je gemaakt hebt.
5. Vraag de AKS credentials op, zodat je in een later stadium dit cluster met *kubectl* kunt bestoken.

Als het goed is draait het cluster nu.  
Maak een Kubernetes Dashboard beschikbaar voor het cluster en browse naar dit dashboard om je cluster te kunnen beheren. Zie https://docs.microsoft.com/en-us/azure/aks/kubernetes-dashboard voor meer informatie.  

### Azure DevOps Pipeline maken
Uitwerking: `03_pipeline_uitwerking.md`

1. Maak een nieuw project in [Azure DevOps portal](https://dev.azure.com). Geef dit project een zelf verzonnen naam (*burgerbuilder* zou een mooie naam zijn, en sluit goed aan bij deze workshop :-))
2. Schakel de *preview feature* ***Multi-stage pipelines*** aan, en ***New Repos landing pages*** uit.
3. Importeer de burgerbuilder repository van de [Gitlab link](https://gitlab.com/techforce1/burger-builder.git).  
4. Maak een pipeline van het type *Azure repos git* en selecteer de juist geïmporteerde repository. Selecteer de subscription ***Azure-abonnement Techforce1*** als daarom gevraagd wordt en je eigen Kubernetes cluster. De applicatie luistert op poort TCP/3000.
5. Zorg ervoor dat de eerste build en deployment worden afgetrapt en wacht tot dit klaar is (duurt ongeveer 3 minuten).

Als alles goed is gegaan draait de *burgerbuilder* in je cluster en is hij beschikbaar op http://A.B.C.D:3000/burger (A.B.C.D is het externe IP adres). Wat dit externe IP adres is kun je achterhalen via het Kubernetes dashboard of via het [Azure Portal](https://portal.azure.com/#blade/HubsExtension/BrowseAll). Controleer dit.

### Maak het veilig
Uitwerking: `04_secure_uitwerking.md`  

De applicatie luistert op poort 3000, en praat plain-text HTTP. Dat is natuurlijk niet veilig genoeg voor deze bijzondere applicatie. Daarom gaan we security opschroeven en dat doen we door er een nginx reverse proxy voor te zetten. Deze maakt gebruik van een Let's Encrypt SSL-certificaat, waardoor de burgerbuilder netjes SSL lijkt te praten.  
Voor alle *kubectl* commando's geldt: gebruik de juiste namespace (dezelfde als waarin de *burgerbuilder* container is gedeployed in het AKS cluster).    
  
1. Maak een frontend service aan die luistert op TCP/80 en het verkeer doorstuurt naar TCP/3000.
2. Maak de volgende [Helm](https://helm.sh) repositories beschikbaar: https://kubernetes-charts.storage.googleapis.com en https://charts.jetstack.io.
3. Maak een namespace met de naam *ingress* en installeer m.b.v. *helm* de package *nginx-ingress* uit de repository https://kubernetes-charts.storage.googleapis.com.
4. Achterhaal het externe IP adres van de *nginx ingress controller*.
5. Maak een *ingress* resource aan, zodat de burgerbuilder bereikbaar wordt op de hostname ***frontend.<INGRESS_CONTROLLER_EXTERNAL_IP>.nip.io***.
6. Installeer m.b.v. *Helm* de package *cert-manager* uit de repository https://charts.jetstack.io. Zie https://cert-manager.io/docs/installation/kubernetes voor meer informatie.
7. Vraag een Let's Encrypt ACME certificaat aan via de cert-manager. Zie https://cert-manager.io/docs/configuration/acme voor meer informatie.
8. Update de *frontend ingress* resource, zodat nu TLS gebruikt gaat worden.

Je kunt nu burgers gaan bestellen op een veilige manier via https://frontend.A.B.C.D.nip.io/burger, waarbij A.B.C.D het externe IP-adres is.
