## 3. Pipeline maken
- Maak een nieuw project in [Azure DevOps portal](https://dev.azure.com). Geef dit project een zelf verzonnen naam (*burgerbuilder* zou een mooie naam zijn, en sluit goed aan bij deze workshop :-))
  - Om straks succesvol naar een Kubernetes-cluster te kunnen deployen, moet je - voor je een pipeline maakt - een preview-feature activeren. Deze vind je onder één van de icoontjes rechtsboven in. De feature die je moet activeren heet ***Multi-stage pipelines***.  
  - Er is ook een preview-feature die je uit moet schakelen: ***New Repos landing pages***. Als je dat niet doet krijg je een **reactjs** foutmelding bij het importeren van een git repository (en dat ga je straks doen).  

- Importeer de burgerbuilder repository van de [Gitlab link](https://gitlab.com/techforce1/burger-builder.git).  

- Maak een pipeline van het type ***Azure repos git***.  
  - Er zijn niet veel keuzes, je hebt er maar eentje geïmporteerd.
  - We gaan deployen naar een AKS cluster
    - Selecteer je subscription (je hebt maar toegang tot 1 subscription) als daar om gevraagd wordt.
    - Uiteraard pak je je eigen cluster en je eigen registry. De applicatie luistert op poort 3000. Gebruik de ***default*** namespace.
    - Valideer de configuratie als alles ingevuld is.
   - Er wordt een yaml-file gegenereerd, die naar git gepushed wordt. Klik op ***save and run*** om deze te accepteren.  

- De eerste build wordt nu afgetrapt. Dit kan 3 à 4 minuten duren. Daarna wordt de verse container automatisch gedeployed naar het kubernetes cluster (< 1 minuut).  
Als je daarna `kubectl get pods` in je *cloudshell* typt, zul je - als het goed gegaan is - daar je nieuwe container zien draaien.  
Als je het Kubernetes Dashboard hebt draaien, zie je hem daar ook in verschijnen.  

Wanneer de container draait op het AKS cluster, zijn er meerdere manier om te achterhalen hoe je deze via de browser kunt bereiken:
- Ga naar de Azure portal. Selecteer ***all resources***. Je ziet niet alleen je eigen spullen, maar ook die van je mede-workshoppers. Daarom is het handig om de zaken te groeperen, en omdat je de enige bent op jouw *location* is dat een logische keuze.
  - Ga op zoek naar de load balancer en kijk welke publieke IP-adressen daarvoor gemaakt zijn. Een van beiden is de service die net uitgerold is, je kunt hem herkennen aan het feit dat er een rule aan gekoppeld is. Het IP-adres dat daarbij hoort is te bereiken via je browser op poort *TCP/3000*.
- Ga in je Kubernetes Dashboard naar *services* onder *Discovery and Load Balancing*. Uiteraard is dit alleen zichtbaar wanneer je de juiste *namespace* selecteert. 
  - Het *External Endpoint* van de *burgerbuilder* service is het publieke adres (+ poort) waarop de service bereikbaar is.

Als het bovenstaande gelukt is, kun je nu op een onveilige manier een burger samenstellen.  

### Als je er niet uitkomt
Helaas zijn hier geen scripts voor omdat het allemaal in een webinterface gebeurt.