## 2. Kubernetes cluster
We gaan eerst een Kubernetes cluster maken. Daar gaan we straks naar toe deployen. Dit cluster moet draaien in jouw *locatie*. 
Je mag het cluster een zelf verzonnen naam geven. Voer daarvoor de volgende commando's uit in jouw cloudshell. In onderstaande voorbeelden wordt de prefix *tf1ws* gebruikt en de locatie *eastasia*. In jouw situatie zal dat iets anders zijn.  

1. Resource-group maken  
`az group create --name tf1wsgroup --location eastasia`

2. Welke kubernetes versies ondersteunt deze regio?  
`az aks get-versions -l  eastasia --query 'orchestrators[].orchestratorVersion' -o tsv`
  
3. Selecteer de meest recente 1.15.x versie. Versie 1.16.x en hoger hebben wijziging in de API die (nog) niet ondersteund worden door Azure Pipelines.  
`version=1.15.7`

4. Maak de Azure Container Registry aan  
`az acr create --resource-group tf1wsgroup --name tf1wsacr --sku Basic`

5. Bouw het kubernetes cluster, duurt ongeveer 5 minuten (koffie!)  
`az aks create --resource-group tf1wsgroup --kubernetes-version $version --name tf1wsaks --node-count 2 --generate-ssh-keys --attach-acr tf1wsacr`

6. Vraag kubernetes credentials op die je later met kubectl kunt gebruiken  
`az aks get-credentials --resource-group tf1wsgroup --name tf1wsaks`

7. Om wat eenvoudiger het kubernetes-cluster - waar de containers in draaien - te kunnen beheren, ga je een Kubernetes Dashboard beschikbaar maken.  
`kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard`  
`az aks browse --name tf1wsaks --resource-group tf1wsgroup`  

Via de URL die je te zien krijgt, kun je je Kubernetes cluster beheren.  

### Als je er niet uitkomt
In onderstaand voorbeeld wordt het Kubernetes cluster aangemaakt in *eastasia* en worden alle resource-namen voorzien van de prefix *tf1ws*. Dit is alleen een voorbeeld, gebruik je eigen locatie en prefix.
```
cursist@Azure:~/azurecicd/scripts/burgerbuilder$ ./create_aks.sh

Enter location (enter to list locations): eastasia

Enter prefix for resources to be created: tf1ws

Creating resource group tf1wsgroup in location eastasia ... OK
Creating Azure Container Registry tf1wsacr ... OK
Creating Kubernetes cluster tf1wsaks (this may take a while) ... OK
Retrieving Kubernetes credentials ... OK
Configuring access to Kubernetes dashboard ... OK

Execute the following command to get access to the Kubernetes dashboard:
az aks browse --name "tf1wsaks" --resource-group "tf1wsgroup"
```

Voer met de hand het commando uit zoals het script aangeeft.