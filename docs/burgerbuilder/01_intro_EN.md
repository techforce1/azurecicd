## Burgerbuilder
**Or: how to build a hamburger?**

Now you're aware of pipelines in Azure, it's time to build some stuff on your own.

Targets:
- build a container and deploy it on a Kubernetes cluster in Azure;
- inside the container a web-application is running;
- the web interface of the application is publicly available;
- you can compose a hamburger in the webinterface.

The building and deploying parts will be automated and triggered by a change in the code (git repository).

The Azure Kubernetes Service will also be used in the next assignment (Nameko).

Because some of you are working in the same Azure environment (region), it's possible to damage each others work: please don't! 😏
Every region has some restrictions, which includes the maximum number of some resources you can create. So please do not create resources in other regions.

Helpfull information for this assignment:
- [Gitlab repo burgerbuilder](https://gitlab.com/techforce1/burger-builder.git)
- [Azure DevOps portal](https://dev.azure.com)
- [Azure portal](https://portal.azure.com/#home)
- [Azure cloudshell](https://shell.azure.com)
- [Azure cloudshell howto](https://gitlab.com/techforce1/azurecicd/blob/master/docs/cloudshell_EN.md)

## Accessing Azure
- The accountname is the provided *username*, followed by *@infotechforce1.onmicrosoft.com*
- Point your browser to the [Azure portal](https://portal.azure.com/#home). Changing the password is mandatory
- Point your browser (use a second tab/browser) to [CloudShell](https://shell.azure.com)
  - Select **bash** as the preferred shell
  - The *subscription name* is displayed. Select **Create Storage**
- Point your browser (use a third tab/browser) to [Azure DevOps](https://dev.azure.com)
  - You already have an account, so click on the correct hyperlink
  - next-next-finish the login-wizard

### Advanced
*burgerbuilder_advanced.md* just contains essential information for this assignment, no copy/paste solutions. It's more challenging, you have to connect the dots on your own.

### Just tell me what to do
The documents *02_kubcluster_solution.md*, *03_pipeline_solution.md*, *04_secure_solution.md* contain all the steps to complete this assignment.

### Quickies
When you're in a hurry, there is a fast-lane to the solution. The bash-scripts are available in this repository in the directory *scripts*.
The *solution*-documentation displays how to start the scripts.
The use them, you must clone this git-repository to your Cloudshell and move to the directory *azurecicd/scripts/burgerbuilder*
```
git clone https://gitlab.com/techforce1/azurecicd.git
cd azurecicd/scripts/burgerbuilder
```
