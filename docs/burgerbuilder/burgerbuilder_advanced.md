# Advanced burgers
Every resource you create must apply to the following rules:
- Create them in you own *region*
  - You create the resource group in a *region*, and the resources in the *resource group*
- Use a custom prefix so you can easily identify your resources


### Create an Azure Kubernetes Service cluster
Solution: `02_kubcluster_solution.md`

The following can be executed by using the Azure CLI (command `az`).
1. Create a resource group. The resources you create after this step must be part of this resource group.
2. Determine the newest 1.15.x Kubernetes version available in your *region*. 
3. Create a container registry (*ACR*) using the *service tier* (SKU) ***Basic***.
4. Create an AKS cluster, version *1.15.x* (version 1.16+ has breaking API-changes).
5. Retrieve the AKS credentials that can be used to access the cluster with *kubectl* later on.

When it works out fine, the cluster is running now.  
Make a Kubernetes dashboard available for managing the cluster, and point your browser to the dashboard. Check https://docs.microsoft.com/en-us/azure/aks/kubernetes-dashboard for more information.  

### Create an Azure DevOps Pipeline
Solution: `03_pipeline_solution.md`

1. Create a  project in [Azure DevOps portal](https://dev.azure.com). Name it as you which (*burgerbuilder* fits perfectly fine).
2. Enable *preview feature* ***Multi-stage pipelines***, disable ***New Repos landing pages***.
3. Import the [burgerbuilder](https://gitlab.com/techforce1/burger-builder.git) git repository.  
4. Create a *Azure repos git* pipeline and select the imported repository. Select your subscription (there is only one) and your own Kubernetes cluster. The application will listen at port 3000.
5. Make sure the build and deployment steps are executed and wait for it to finish. This will take approximatly 3 minutes.

The *burgerbuilder* container now runs in your Kubernetes cluster and is accessible at http://A.B.C.D:3000/burger (A.B.C.D is the external IP adsress). You can retrieve this external IP address in the Kubernetes dashboard of in the [Azure Portal](https://portal.azure.com/#blade/HubsExtension/BrowseAll). Please check this.

### Secure the burgerbuilder
Solution: `04_secure_solution.md`  

The application is listening on port TCP/3000 and speaks plain-text HTTP. Ofcourse we require such an important application to be secure.  
You will deploy an *nginx* reverse proxy which uses an Let's Encrypt SSL-certificate, which makes the *burgerbuilder* seem to speak SSL/TLS.  
Use the correct *namespace* when you issue *kubectl* commands. This is the same *namespace* you used in the pipeline when the *burgerbuilder* container was deployed.  

1. Create a frontend service which is listening on port TCP/80 and forwards the traphic to TCP/3000.
2. Make two [Helm](https://helm.sh) repositories available: https://kubernetes-charts.storage.googleapis.com and https://charts.jetstack.io.
3. Add a namespace named *ingress* and install (using *helm*) package *nginx-ingress* from repository https://kubernetes-charts.storage.googleapis.com.
4. Retrieve the external IP address of the *nginx ingress controller*.
5. Create an *ingress* resource which makes burgerbuilder available at hostname ***frontend.<INGRESS_CONTROLLER_EXTERNAL_IP>.nip.io***.
6. Install (using *Helm*) package *cert-manager* (repository https://charts.jetstack.io). Check https://cert-manager.io/docs/installation/kubernetes for more information.
7. Order cert-manager to issue a Let's Encrypt ACME certificate. Check https://cert-manager.io/docs/configuration/acme for more information.
8. Update the *frontend ingress* resource to force it to use TLS.

Now you can order secure burgers at https://frontend.A.B.C.D.nip.io/burger (A.B.C.D is the external IP address).
